ARG PHP_VERSION=8.2.6
FROM php:${PHP_VERSION}-fpm-alpine

ONBUILD ARG ENABLE_GIT=n

ONBUILD RUN apk upgrade --available; \
  if [[ "$ENABLE_GIT" = "y" ]]; then \
    apk add --no-cache git; \
  fi
